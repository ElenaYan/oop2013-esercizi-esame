package esami2013.appello05.e3;

public interface Function<I,O> {
	O apply(I i);
}
